import express from 'express';

import {addTodo, deleteTodo, getTodos, updateTodo} from "../controller";

export default (router: express.Router) => {
    router.get("/", getTodos)

    router.post("/add", addTodo)

    router.put("update/:id", updateTodo)

    router.delete("delete/:id", deleteTodo)
};
