import express, { Express, Request, Response } from 'express';


import http, { Server } from 'http';
import mongoose from "mongoose";
import router from "./route";

const index: Express = express();

index.use(express.json());

index.use('/todos/',router());

const port = process.env.port || 3000;

const server: Server = http.createServer(index);server.listen(port, () => {

    console.log(`Server is running at http://localhost:${port}`);

});

const MONGO_URL = 'mongodb+srv://cnh:chiheb@todo.jzr2yum.mongodb.net/'; // DB URI

mongoose.Promise = Promise;
mongoose.connect(MONGO_URL);
mongoose.connection.on('error', (error: Error) => console.log(error));

