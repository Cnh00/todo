import {Response,Request} from "express";
import {ITodo} from "../entity/todo";
import Todo  from "../model/todoSchema";


//get
const getTodos= async (req : Request , res :Response)=> {
    try {
        const todolist: ITodo[]= await Todo.find()
        return res.status(200).json(todolist)
    }
    catch (error) {
        console.log(error);
        return res.sendStatus(400);

    }
}


//add

const addTodo = async (req : Request, res : Response) => {
    try {
        const { title, description } = req.body;


        const newTodo = new Todo({
            title,
            description,

        });

        await newTodo.save();

        return res.status(201).json(newTodo);
    } catch (error) {
        console.log(error);
        return res.sendStatus(400);
    }
};



//update
const updateTodo= async (req:Request,res:Response)=> {
    try {
        const {
            params:{id},
            body,
        }=req
        const updateTodo : ITodo | null= await Todo.findByIdAndUpdate(
            {_id:id},
            body
        )
        const allTodos:ITodo[]= await Todo.find()
        res.status(200).json({
            message:"Todo updated",
            todo:updateTodo,
            todos:allTodos
        })

    } catch (error) {
        console.log(error);
        return res.sendStatus(400);
    }
}


//delete
const deleteTodo = async (req : Request , res : Response ) => {
    try {
        const deletedTodo : ITodo | null = await Todo.findByIdAndRemove(
            req.params.id
        )

        const allTodos: ITodo[] = await Todo.find()
        res.status(200).json({
            message: "Todo deleted",
            todo: deletedTodo,
            todos: allTodos,
        })
    }catch (error) {
        console.log(error);
        return res.sendStatus(400);
    }
}

export { getTodos, addTodo, updateTodo, deleteTodo }




























